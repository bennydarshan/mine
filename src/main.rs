extern crate termion;

use termion::color;
use termion::event::Key;
use termion::input::TermRead;
use termion::raw::IntoRawMode;

use std::io::{Write, stdin, stdout};

use rand::prelude::*;

const DEMS: usize = 128;

#[derive(Copy, Clone)]
enum Cell{
    Mine,
    Empty(u8)
}
#[derive(Copy, Clone)]
struct Piece{
    data: Cell,
    show: bool,
    flag: bool
}

struct Board{
    w: usize,
    h: usize,
    m: u32,
    closed: usize,
    board: [[Piece;DEMS];DEMS]
}

impl Board{
    pub fn new(w: usize, h: usize, mut m: u32) -> Board {
        let mut ret = Board{m: m, closed: w*h, w: w, h: h, board: [[Piece{data: Cell::Empty(0), show: false, flag: false};DEMS];DEMS]};
        let mut rng = rand::thread_rng();
        while m > 0 {
            let x: usize = rng.gen();
            let x = x % (w * h);
            let mut cur = &mut ret.board[x/w][x%w];
            match cur.data {
                Cell::Mine => continue,
                _ => {
                    cur.data = Cell::Mine;
                    m = m - 1;
                }
            }

        }
        for i in 0..ret.h {
            for j in 0..ret.w {
                if let Cell::Mine = ret.board[i][j].data {
                    continue;
                }
                let mut count = 0;
                for (x, y) in ret.neighbors(j, i) {
                    if let Cell::Mine = ret.board[y][x].data {
                        count += 1;
                    }

                }
                ret.board[i][j].data = Cell::Empty(count);
            }
        }
        return ret;
    }
    fn neighbors(&self, x: usize, y:usize) -> Vec<(usize, usize)> {
        let mut ret = Vec::<(usize, usize)>::new();
        let offsets: [i128; 3] = [1, -1, 0];
                for offi in offsets.iter() {
                    for offj in offsets.iter() {
                        let x: i128 = x as i128 + offi;
                        if x < 0 || x >= self.w as i128 {
                            continue;
                        }
                        let x: usize = x as usize;
                        let y: i128 = y as i128 + offj;
                        if y < 0 || y >= self.h as i128 {
                            continue;
                        }
                        let y: usize = y as usize;
                        ret.push((x, y));
                    }
                }

        ret
    }
    pub fn expose(&mut self) {
        for itery in self.board.iter_mut() {
            for iterx in itery.iter_mut() {
                if let Cell::Mine = iterx.data {
                    iterx.show = true;
                }
            }
        }
    }
    pub fn flag(&mut self, x: usize, y: usize) {
        let mut cur = &mut self.board[y][x];
        if let true = cur.show {
            return;
        }
        cur.flag = !cur.flag;

    }
    pub fn open(&mut self, x: usize, y: usize) -> Option<bool> {
        if self.board[y][x].show == true {
            self.closed += 1;
        }
        self.board[y][x].show = false;
        return self._open(x, y);
    }
    fn _open(&mut self, x: usize, y: usize) -> Option<bool> {
        let mut cur = &mut self.board[y][x];
        if let true = cur.show {
            return None;
        }
        if let true = cur.flag {
            return None;
        }
        cur.show = true;
        self.closed-= 1;
        if self.closed == self.m as usize {
            return Some(false);
        }
        match cur.data {
            Cell::Mine => {return Some(true);},
            Cell::Empty(0) => (),
            Cell::Empty(n) => {
                let mut count = 0;
                for (_x, _y) in self.neighbors(x, y) {
                    if let true = self.board[_y][_x].flag {
                        count += 1;
                    }
                }
                if n > count {
                    return None;
                }

            }
        }
        for (_x, _y) in self.neighbors(x, y) {
            match self._open(_x, _y) {
                Some(true) => return Some(true),
                Some(false) => return Some(false),
                None => ()
            }
        }
        None


    }

    pub fn print(&self) {
        print!("{}{}", color::Bg(color::Reset), termion::clear::All);
        for i in 0..self.h {
            for j in 0..self.w {
                let cur = self.board[i][j];
                print!("{}{}{}", color::Bg(color::Reset), color::Fg(color::Reset), termion::cursor::Goto(j as u16 + 1, i as u16 + 1));
                match cur.show {
                    false => match cur.flag {
                        false => print!("{} ", color::Bg(color::White)),
                        true => print!("{}{}?", color::Bg(color::White), color::Fg(color::Green)),
                    },
                    true => {
                        match cur.data {
                            Cell::Mine => print!("{}X", color::Fg(color::Red)),
                            Cell::Empty(x) => print!("{}{}",color::Fg(color::Blue), x),
                        }
                    }
                }
            }
            print!("{}", '\n');
        }
    }
}

fn main() {
    let stdin = stdin();
    let mut failed = false;

    let mut input = String::new();
    let mut params = [0usize;3];
    println!("please insert w h m");
    'read: loop {
        stdin.read_line(&mut input).expect("Can't read text!");
        for (i, val) in input.split(' ').enumerate() {
            params[i] = match val.trim().parse() {
                Ok(x) => x,
                Err(_) => {
                    println!("Please insert numbers");
                    continue 'read
                }
            }
        }
        break;
    }

    let mut stdout = stdout().into_raw_mode().unwrap();

    let mut x = 0u16;
    let mut y = 0u16;
    let mut b = Board::new(params[0], params[1], params[2] as u32);
    for ch in stdin.keys() {
        match ch.unwrap() {
            Key::Esc => break,
            Key::Up => if y > 0 {y -= 1},
            Key::Down => if y + 1 < b.h as u16 { y += 1},
            Key::Left => if x > 0 {x -= 1},
            Key::Right => if x + 1 < b.w as u16 {x += 1},
            Key::Char('c') => b.flag(x as usize, y as usize),
            Key::Char(' ') => {
                match b.open(x as usize, y as usize) {
                    Some(true) => {
                        failed = true;
                        b.expose();
                        break;
                    },
                    Some(false) => {
                        failed = false;
                        //b.expose();
                        break;
                    },
                    None => ()
                }
            },
            _ => continue
        }
        b.print();
        write!(stdout, "{}", termion::cursor::Goto(x + 1, y + 1)).unwrap();
        stdout.flush().unwrap();

    }
    b.print();
    print!("{}", termion::cursor::Goto(1, b.h as u16 + 1));

    print!("{}{}", color::Bg(color::Reset), color::Fg(color::Reset));
    if failed {
        println!("You died.");
    } else {
        println!("You win!");
    }
}
